#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
void upcase(char *s){                       //pone a Mayusculas el nombre de Señal
    while (*s){
        *s = toupper(*s);
        s++;
    }
}

void trapper(int sig){                      //funcion que imprime la señal que recibe el proceso actual
 char * str = strdup(sys_siglist[sig]);
    if (!str)
        exit(EXIT_FAILURE);
    upcase(str);
printf("\n¡¡Se envío la señal: ");
    printf("%2d -> SIG%s!!\n\n\n\n", sig, str);
    free(str);
	if(sig == SIGINT || sig == SIGTSTP)     //para terminar el proceso se puede usar SIGINT (Ctrl-C) o SIGTSTP (Ctrl-Z)
		exit(EXIT_SUCCESS);
}
int main(){
	int i=1;
    printf("PROCESO NUEVO: %d\n", getpid());    //muestra pid del proceso actual
	while(1){
        for(int i=1; i<=64; i++)
            signal(i, trapper);                 //manejador de señal
		printf("\nContando... %d\n",i);
		sleep(5);
		i++;
	}
	return 0;
}
