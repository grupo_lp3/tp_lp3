#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define N 100
typedef struct senales{
	int pid;
	int senal;
	int delay;
}Senal;

Senal sig[N];							//variables globales
int i=0;

int compare (const void * a, const void * b){                   //funcion para comparar
        const Senal *sa = a;
        const Senal *sb = b;
        return ((sa->delay) -(sb->delay));
}

int main(){
	FILE *fp;
	fp= fopen("file.txt", "r");
	while(fscanf(fp, "%d %d %d", &sig[i].pid, &sig[i].senal, &sig[i].delay) && !feof(fp))	//se lee desde el archivo file.txt
		i++;
	rewind(fp);
	qsort(sig,i,sizeof(Senal),compare);		//se ordena por delay
	int j=0;
	int zero=0;
	for(j=0;j<i;j++){
		sleep(sig[j].delay-zero);		//espera n segundos
		if(sig[j].pid>0 && kill(sig[j].pid, sig[j].senal)==0){	//si el pid no es cero y se realiza con exito
			printf("PID: %d con SENAL: %d se llevo a cabo despues de %d segundos\n", sig[j].pid, sig[j].senal, sig[j].delay);
		}else{
			printf("PID: %d con SENAL: %d no se pudo llevar a cabo\n", sig[j].pid, sig[j].senal);
		}
		zero=sig[j].delay;
	}
	fclose(fp);
	return 0;
}
